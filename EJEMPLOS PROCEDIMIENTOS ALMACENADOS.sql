﻿/* EJEMPLO1 
  CREAR UN PROCEDIMIENTO ALMACENADO QUE ME MUESTRE LOS NOMBRES DE LOS 
  CICLISTAS
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo1 $$
CREATE PROCEDURE ejemplo1 ()
BEGIN
   SELECT DISTINCT c.nombre FROM ciclista c;
END
$$

DELIMITER ;

CALL ejemplo1();


/* EJEMPLO2 
  CREAR UN PROCEDIMIENTO ALMACENADO QUE ME MUESTRE el nombre del ciclista que ha ganado mas puertos
  CICLISTAS
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo2 $$
CREATE PROCEDURE ejemplo2 ()
BEGIN

  
  SELECT  c.nombre FROM ciclista c JOIN (
  SELECT p.dorsal,COUNT(*) nPuertos 
    FROM puerto p GROUP BY p.dorsal HAVING nPuertos=(
  SELECT MAX(c1.nPuertos) FROM  
  (SELECT p.dorsal,COUNT(*) nPuertos 
    FROM puerto p GROUP BY p.dorsal) c1))c2 ON c.dorsal=c2.dorsal;

END
$$

DELIMITER ;

-- UTILIZANDO VARIABLES EN EL EJEMPLO

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo2 $$
CREATE PROCEDURE ejemplo2 ()
BEGIN
  DECLARE numeroPuertos int;

  -- en esta variable almaceno el numero de puertos que gana el ciclista que gana mas puertos
  SET numeroPuertos=(
  SELECT MAX(c1.nPuertos) FROM  
  (SELECT p.dorsal,COUNT(*) nPuertos 
    FROM puerto p GROUP BY p.dorsal) c1);

  
  SELECT  c.nombre FROM ciclista c JOIN (
  SELECT p.dorsal,COUNT(*) nPuertos 
    FROM puerto p GROUP BY p.dorsal HAVING nPuertos=numeroPuertos)c2 ON c.dorsal=c2.dorsal;

END
$$

DELIMITER ;

CALL ejemplo2();



/* 
  EJEMPLO3 
  CREAR UN PROCEDIMIENTO QUE CONTENGA TRES VARIABLES
  UNA=10;
  DOS=5;
  TRES=2;
  QUIERO QUE ME MUESTRE LA SUMA DE LAS 3 VARIABLES, LA RESTA DE LAS TRES VARIABLES Y EL PRODUCTO
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo3 $$
CREATE PROCEDURE ejemplo3 ()
BEGIN
  DECLARE una int DEFAULT 10;
  DECLARE dos int DEFAULT 5;
  DECLARE tres int;

  SET tres=2;

  SELECT una+dos+tres AS Suma, una-dos-tres AS Resta, una*dos*tres AS Producto; 

END
$$

DELIMITER ;

CALL ejemplo3();

/*
  EJEMPLO4
  NOMBRE DEL CICLISTA MAS VIEJO 
*/

/**
  Podemos utilizar cualquiera de las 3 opciones
**/

SELECT nombre FROM ciclista c WHERE c.edad=
(SELECT MAX(c.edad) edadMaxima FROM ciclista c);


SELECT c.nombre FROM ciclista c 
  JOIN (SELECT MAX(c.edad) edadMaxima FROM ciclista c) c1 
  ON c.edad=c1.edadMaxima;

SELECT c.nombre FROM ciclista c ORDER BY c.edad DESC LIMIT 1;

-- creo el procedimiento

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo4 $$
CREATE PROCEDURE ejemplo4 ()
BEGIN
  DECLARE edadMaxima int DEFAULT 0;

  SET edadMaxima=(SELECT MAX(c.edad) AS maxima FROM ciclista c);  

  SELECT nombre FROM ciclista c WHERE c.edad=edadMaxima;

END
$$

DELIMITER ;

CALL ejemplo4();

/*
  EJEMPLO 5
  CREAR UN PROCEDIMIENTO QUE LE PASO UN NUMERO (PARAMETRO) Y ME TIENE QUE INDICAR LA 
  NOTA EN TEXTO. EL NUMERO ES UNA NOTA Y ME TIENE QUE MOSTRAR:
  NOTA <5 ==> SUSPENSO
  NOTA>=5 ==> APROBADO

  CALL EJEMPLO5(7) ==> APROBADO
  CALL EJEMPLO5(3) ==> SUSPENSO
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo5 $$
CREATE PROCEDURE ejemplo5 (numero int) -- argumento
BEGIN
  -- variable
  DECLARE resultado varchar(20) DEFAULT "APROBADO";

  IF (numero<5) THEN
    set resultado="SUSPENSO";
  END IF;

  SELECT resultado;

END $$
DELIMITER ;

CALL ejemplo5(6);

/*
  EJEMPLO 6
  CREAR UN PROCEDIMIENTO QUE LE PASO UN NUMERO (PARAMETRO) Y ME TIENE QUE INDICAR LA 
  NOTA EN TEXTO. EL NUMERO ES UNA NOTA Y ME TIENE QUE MOSTRAR:
  NOTA <5 ==> SUSPENSO
  NOTA >=5 Y NOTA<7 ==> APROBADO
  NOTA>=7 ==> SOBRESALIENTE

  CALL EJEMPLO6(7) ==> SOBRESALIENTE
  CALL EJEMPLO6(6) ==> APROBADO
  CALL EJEMPLO6(2) ==> SUSPENSO
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo6 $$
CREATE PROCEDURE ejemplo6 (numero int)
BEGIN
  DECLARE resultado varchar(20) DEFAULT "SOBRESALIENTE";

  IF (numero<5) THEN
    SET resultado="SUSPENSO";
  ELSEIF (numero<7) THEN
    SET resultado="APROBADO";
  END IF;

  SELECT resultado;

END
$$

DELIMITER ;

CALL ejemplo6(8);
 

/*
  EJEMPLO 7 
  CREAR UN PROCEDIMIENTO QUE LE PASO UN NUMERO (PARAMETRO) Y ME TIENE QUE INDICAR LA 
  NOTA EN TEXTO. EL NUMERO ES UNA NOTA Y ME TIENE QUE MOSTRAR:
  NOTA <5 ==> SUSPENSO
  NOTA >=5 Y NOTA<7 ==> APROBADO
  NOTA>=7 ==> SOBRESALIENTE

  REALIZARLO CON CASE

  CALL EJEMPLO6(7) ==> SOBRESALIENTE
  CALL EJEMPLO6(6) ==> APROBADO
  CALL EJEMPLO6(2) ==> SUSPENSO

*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo7 $$
CREATE PROCEDURE ejemplo7 (numero int)
BEGIN
  DECLARE resultado varchar(20);

  CASE 
    WHEN (numero>=7) THEN 
      SET resultado="SOBRESALIENTE";
    WHEN (numero>=5) THEN 
      SET resultado="APROBADO";
    ELSE
      set resultado="SUFICIENTE";
  END CASE;
  
  SELECT resultado;

END
$$

DELIMITER ;

CALL ejemplo7(6);


    
/* 
  EJEMPLO 8
  CREAR UN PROCEDIMIENTO ALMACENADO QUE ME PERMITA CREAR UNA TABLA DONDE ME COLOCARA LOS 
  SIGUIENTES CAMPOS:
  NOTAS(
  id int PRIMARY KEY,
  nota int,
  valor varchar(50))
*/




/* 
  EJEMPLO 9
  CREAR UN PROCEDIMIENTO ALMACENADO QUE LE PASAS UN NUMERO Y DEBE INTRODUCIRLO EN LA TABLA CREADA
  LLAMADA NOTAS EL NUMERO PASADO EN EL CAMPO NOTA Y DEBE INTRODUCIR LA NOTA COMO TEXTO EN EL 
  CAMPO VALOR SEGUN EL SIGUIENTE BAREMO
  NOTA<5 ==> SUSPENSO
  5<=NOTA<7 ==> APROBADO
  7<=NOTA<9 ==> NOTABLE
  NOTA>=9 ==> SOBRESALIENTE
**/
   